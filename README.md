# tlmprogram

"ME" refers to the Lord of the Rings books. I mean if you care about that
kind of stuff I mean. Like if you like fantasy, why haven't you read them
yet? Oh sure they're long, but this community also says things like "if
a video game isn't at least 100 hours long it isn't worth it." That's totally
a double standard of books vs video games. It'll still take less time out of
your life to read them than playing Skyrim for the 8th time since they re-
released it AGAIN. C'mon ya'll. They're actual classics. They're just not
institutionally recognized as such because most of academia can't recognize
meaning or symbolism unless the author literally slaps them in the face with
it.
